---
layout: layout.html
---

<% set('title', 'Announcing Bitcoin Cash Node v27.0.0') %>
<% set('date', '13 December 2023') %>
<% set('author', 'Bitcoin Cash Node Team') %>

### Release announcement: Bitcoin Cash Node v27.0.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its major release version 27.0.0.

This release implements the May 15, 2024 network upgrade.

It delivers the Adaptive Blocksize Limit Algorithm consensus change:

- CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash (git hash `ba9ed768` of 19 Nov 2023)

and a number of other enhancements, bugfixes and performance improvements.

BCHN users should consider an update prior to May 15, 2024 as mandatory.  The v25.0.0 and v26.x.0 software will expire on May 15, 2024, and will start to warn of the need to update ahead of time, from April 15, 2024 onward.

For the full release notes, please visit:

[https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v27.0.0](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v27.0.0)

Executables and source code for supported platforms are available at the above link, or via the download page on our project website at

[https://bitcoincashnode.org](https://bitcoincashnode.org)

We hope you enjoy our latest release and invite you to join us to improve Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
