---
layout: layout.html
---

<% set('title', 'BCHN v26.0.0版本发布公告') %>
<% set('date', '2023年1月8日') %>
<% set('author', 'Bitcoin Cash Node Team') %>

## BCHN v26.0.0版本发布公告

Bitcoin Cash Node (BCHN) 项目在此宣布其重要版本更新 26.0.0 的发布。

此次 BCHN发布被标记为一个重大版本，并不是因为共识改变，而是因为它出于安全原因改变了一个介面 (rpcbind/rpcallowip 选项)，这个介面改变并不完全向后兼容，因此需要根据Semantic Versioning标记为重大版本。

然而从实用意义上看，本版更像是一个次要版本，包含了修正和改进。

如果你使用rpcbind/rpcallowip配置选项，你应该阅读相关的发布说明，以了解这个改变，可能需要调整你的配置。

BCHN 用户应该在2023年5月15日之前升级到v25或更高版本。运行v25的用户不需要升级到v26.0.0，但我们建议这样做。

v24.x.0软件将在2023年5月15日过期，并将从2023年4月15日开始警告用户。

查看完整的发布说明，请访问:

[https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v26.0.0](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v26.0.0)

该链接中有可执行程序和支持平台的源码，你也可以在BCHN的项目网页进行下载：

[https://bitcoincashnode.org](https://bitcoincashnode.org)

关于2023年5月15日的网路升级，请查询

[https://upgradespecs.bitcoincashnode.org/2023-05-15-upgrade/](https://upgradespecs.bitcoincashnode.org/2023-05-15-upgrade/)

我们希望最新版本能带给你好的体验。让我们一起改善BCH。

BCHN团队
